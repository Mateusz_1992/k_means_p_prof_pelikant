from k_means import K_Means

"""
Szanowny Panie Profesorze,

zgodnie z tym o co Pan prosił zdecydowałem się zaimplementować dowolny algorytm eksploracji danych wraz z użyciem dowolnej bazy danych pochodzącej z dowolnego repozytorium danych
Baza danych znajduje się po tym adresem, nazywa sie xclara - bivariate Data Set with 3 Clusters:
https://vincentarelbundock.github.io/Rdatasets/datasets.html

Zdecydowałem się na implementację algorytmu k-średnich


"""

def main():
    k_means = K_Means(number_of_clusters=4)

    k_means.read_data()
    k_means.determine_centroids()
    k_means.do_clustering()
    k_means.do_scattering_of_clustered_dataset()




if __name__ == "__main__":
    main()
