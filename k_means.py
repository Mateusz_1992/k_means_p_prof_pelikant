from copy import deepcopy
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


class K_Means():
    """
    Class implementing K_Means algorithm

    ...

    Attributes
    --------------------------------------------------------
   self.number_of_clusters : int
       property representing an interface between user and sensor

   self.is_data_read : boolean
       property representing signal processing of the raw radar signal

    self.data : list

    self.X : numpy array

    self.centroid_coordinates : numpy array

    self.centroid_x : numpy array

    self.centroid_y : numpy array

    self.features_1 : numpy array

    self.features_2 : numpy array

    self.clusters : numpy array

   Methods
    --------------------------------------------------------
    def __init__(self, number_of_clusters = 3)
        constructor of the class

    def read_data(self, filename="xclara.csv")
        method used for data reading

    def scatter_dataset(self):
        method scatters dataset without

    def dist(self, a, b, ax=1)
        method counting the Euclidean distance

    def determine_centroids(self)
        method drawing the centroids

    def scatter_dataset_with_centroids(self)
        method scattering dataset with random centroids

    def do_clustering(self)
        method performing clustering

    def do_scattering_of_clustered_dataset(self)
        method scattering dataset after clustering
    """
    def __init__(self, number_of_clusters = 3):
        self.number_of_clusters = number_of_clusters
        self.is_data_read          = False
        self.data                  = None
        self.X                     = None
        self.centroid_coordinates  = None
        self.centroid_x            = None
        self.centroid_y            = None
        self.features_1            = None
        self.features_2            = None
        self.clusters              = None

    def read_data(self, filename="xclara.csv"):
        self.data = pd.read_csv(filename)
        self.data = self.data.dropna(axis=0)

        self.X = np.array(list(zip(self.data['V1'].values, self.data["V2"].values)))
        print(self.data.shape)
        print(self.data.head())
        print(self.data.tail())

    def scatter_dataset(self):
        self.features_1 = self.data["V1"].values
        self.features_2 = self.data["V2"].values

        plt.scatter(self.features_1, self.features_2, c="black", s=7)

    def dist(self, a, b, ax=1):
        #print("Przed dystansie")
        distance = np.linalg.norm(a - b, axis=ax)
        #print("Po dystansie")
        return distance

    def determine_centroids(self):
        # X coordinates of random centroids
        self.centroid_x = np.random.randint(0, np.max(self.X)-10, size=self.number_of_clusters)
        # Y coordinates of random centroids
        self.centroid_y = np.random.randint(0, np.max(self.X)-10, size=self.number_of_clusters)
        self.centroid_coordinates = np.array(list(zip(self.centroid_x, self.centroid_y)), dtype=np.float32)
        print(self.centroid_coordinates)

    def scatter_dataset_with_centroids(self):
        # Plot data with centroids
        plt.scatter(self.features_1, self.features_2, c='#AA0505', s=7)
        plt.scatter(self.centroid_x, self.centroid_y, marker='*', s=200, c='g')

    def do_clustering(self):
        # Storage for old centroids
        centroid_old = np.zeros(self.centroid_coordinates.shape)
        #Clusters
        self.clusters = np.zeros(len(self.X))

        print("len(self.X)")
        print(len(self.X))
        print("len(self.X)")
        # Count initial distance
        error = self.dist(self.centroid_coordinates, centroid_old, None)

        while error != 0:
            #Choose closest cluster
            for i in range(len(self.X)):
                distances = self.dist(self.X[i], self.centroid_coordinates)

                cluster = np.argmin(distances)
                self.clusters[i] = cluster

            centroid_old = deepcopy(self.centroid_coordinates)
            #Count new centroids
            for i in range(self.number_of_clusters):
                points = [self.X[j] for j in range(len(self.X)) if self.clusters[j] == i]
                self.centroid_coordinates[i] = np.mean(points, axis=0)
            error = self.dist(self.centroid_coordinates, centroid_old, None)

    def do_scattering_of_clustered_dataset(self):
        fig, ax = plt.subplots()
        for i in range(self.number_of_clusters):
            points = np.array([self.X[j] for j in range(len(self.X)) if self.clusters[j] == i])
            c1 = np.random.randint(0,255)
            c1 = str(hex(c1))
            c1 = c1[2:]
            if len(c1) < 2:
                c1 = "0" + c1

            c2 = np.random.randint(0,255)
            c2 = str(hex(c2))
            c2 = c2[2:]
            if len(c2) < 2:
                c2 = "0" + c2

            c3 = np.random.randint(0,255)
            c3 = str(hex(c3))
            c3 = c3[2:]

            if len(c3) < 2:
                c3 = "0" + c3

            color_combined = "#" + c1 + c2 + c3

            print("color_combined")

            print(color_combined)

            print("color_combined")

            ax.scatter(points[:, 0], points[:, 1], s=10, c=color_combined)
        ax.scatter(self.centroid_coordinates[:, 0], self.centroid_coordinates[:, 1], marker='D', s=200, c='#AA0505')

        plt.show()



